import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RcService } from './rc.service';
import { HiveScannerService } from '../scanner/hiveScanner.service';
import { QuotesService } from '../quotes/quotes.service';

@Module({
  providers: [RcService, HiveScannerService, QuotesService, ConfigService],
  exports: [RcService],
})
export class RcModule {}
