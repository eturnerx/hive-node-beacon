import * as hiveRc from '@hiveio/hive-js-rc';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron } from '@nestjs/schedule';
import { vestToHive } from 'src/helperFunctions';
import { HiveScannerService, HiveNodeStatus } from '../scanner/hiveScanner.service';
import { hiveNodes } from '../hiveConfig';

const RC_HISTORY_SIZE = 24;

const getDynamicGlobalProperties = (): Promise<any> => hiveRc.api.callAsync('database_api.get_dynamic_global_properties', {});
const getRcStats = (): Promise<any> => hiveRc.api.callAsync('rc_api.get_rc_stats', {});

const getBestNode = (nodes: HiveNodeStatus[] = []): string => {
  const validNodes: HiveNodeStatus[] = nodes.filter(n => n.score >= 0 && !n.website_only);

  // if there are at least MIN_NODES with the best score -> return
  if (validNodes.length === 0) {
    return hiveNodes[0].endpoint;
  }

  // otherwise reduce the filter threshold
  return validNodes.sort((a, b) => b.score - a.score).shift().endpoint;
};

@Injectable()
export class RcService {
  private readonly logger = new Logger(RcService.name);
  private isRunning = false;
  private rcHistory: any[] = [];

  constructor(private readonly scannerService: HiveScannerService, private configService: ConfigService) {}

  onModuleInit() {
    this.run();
  }

  getRcCosts(): any[] {
    return this.rcHistory[0];
  }

  getRcHistoryCosts(): any[] {
    return this.rcHistory;
  }

  @Cron('*/60 * * * *')
  async run() {
    // skip if disabled
    if (this.configService.get<string>('ENABLE_RC', 'true').toLowerCase() !== 'true') {
      this.logger.warn('RC cost checker disabled -> skip');
      return;
    }

    // skip if already running
    if (this.isRunning) {
      this.logger.warn(`Skip RC run as previous task still running`);
      return;
    }

    this.isRunning = true;

    try {
      const rcHiveNode = getBestNode(this.scannerService.getNodes());
      hiveRc.api.setOptions({ url: rcHiveNode || 'https://api.hive.blog' });
      const dynamicGlobalProperties = await getDynamicGlobalProperties();
      const totalVestingHive = dynamicGlobalProperties.total_vesting_fund_hive.amount;
      const totalVestingShares = dynamicGlobalProperties.total_vesting_shares.amount;

      const rcStats = await getRcStats();

      const currentTime = new Date();
      const testsResults = [];

      for (const op in rcStats.rc_stats.ops) {
        const result = {
          operation: op,
          rc_needed: rcStats.rc_stats.ops[op].avg_cost,
          hp_needed: vestToHive(totalVestingHive, rcStats.rc_stats.ops[op].avg_cost, totalVestingShares),
        };
        this.logger.log(JSON.stringify(result));
        testsResults.push(result);
      }

      const lastTestsCycle = {
        timestamp: currentTime,
        costs: testsResults,
      };
      this.rcHistory.unshift(lastTestsCycle);
      if (this.rcHistory.length > RC_HISTORY_SIZE) {
        this.rcHistory.pop();
      }

      this.logger.log(`RC costs check occurred at ${currentTime}`);
    } finally {
      this.isRunning = false;
    }
  }
}
