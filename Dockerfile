FROM node:16-alpine
WORKDIR /usr/src/app

# Copy files over to container

COPY . .

# Install required modules and build client/server structure

RUN npm install
RUN npm run build

# Expose client and server to host network

EXPOSE 8080
EXPOSE 3000

CMD [ "node", "dist/src/main" ]
